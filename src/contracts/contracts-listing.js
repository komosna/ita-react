import React from 'react'
import {Link} from 'react-router-dom'
import ROUTES from '../routes'

import svc from './contracts-service'

class ContractListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contracts: []
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    let res = await svc.getContracts()

    this.setState({
      contracts: res.data
    })
  }

  render() {
    const contracts = this.state.contracts
    return (
      <div>
        <table className="table">
          <thead>
          <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Contact</th>
          </tr>
          </thead>
          <tbody>
            {contracts.map(s =>
              <tr key={s.id}>
                <td><Link to={ROUTES.getUrl(ROUTES.CONTRACT_DETAIL, {id: s.id})}>{s.name}</Link></td>
                <td>{('' + s.description).slice(0, 60)}</td>
                <td>{s.price}</td>
                <td>
                  <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, {id: s.contact.id})}>{s.contact.name}</Link>
                </td>
              </tr>
            )}
          </tbody>
        </table>
        <div>
          <Link to={ROUTES.CONTRACT_NEW} className="btn btn-light">Create new</Link>
        </div>
      </div>

    )
  }
}

export default ContractListing
