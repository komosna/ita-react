import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app';
import authService from './auth-service'

const render = () =>
    ReactDOM.render(<App />, document.getElementById('root'));

authService.init()

render()
authService.onChange = render

window.click = render
window.oninput = render
