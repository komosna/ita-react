export default {
  CONTACT_LISTING: '/contacts',
  CONTACT_NEW: '/contacts/new',
  CONTACT_DETAIL: '/contacts/detail/:id',
  CONTACT_EDIT: '/contacts/edit/:id',

  USER_LISTING: '/users',
  USER_NEW: '/users/new',
  USER_DETAIL: '/users/detail/:id',
  USER_EDIT: '/users/edit/:id',

  CONTRACT_LISTING: '/contracts',
  CONTRACT_NEW: '/contracts/new',
  CONTRACT_DETAIL: '/contracts/detail/:id',
  CONTRACT_EDIT: '/contracts/edit/:id',

    getUrl(path, params = {}) {
      return path.replace(/:(\w+)/g, (m, k) => params[k])
    }
}
