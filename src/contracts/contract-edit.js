import React from 'react'
import ROUTES from '../routes'

import svc from './contracts-service'
import contactService from './../contacts/contacts-service'

import ContractForm from './contract-form'

class ContractEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: null,
      contacts: []
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    svc.getContract(id).then(res => {
      this.setState({
        contract: res.data
      })
    })

    let res = await contactService.getContacts()
      this.setState({
        contacts: res.data
    })
  }
  update() {
    svc.updateContract(this.state.contract).then(res => {
      this.props.history.push(ROUTES.CONTRACT_LISTING)
    })


  }

  render() {
    const contract = this.state.contract
    const contacts = this.state.contacts

    return (
      <div>
        <h2>Edit</h2>
        {contract && <ContractForm contract={contract} contacts={contacts} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Save</button>
      </div>

    )
  }
}

export default ContractEdit
