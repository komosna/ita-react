const express = require('express')
const cors = require('cors')
const jsonServer = require('json-server')

const app = express()


let user = null


app.use(cors())

app.get('/user', (req, res) => {
  res.send(user || 401)
})

app.post('/login', (req, res) => {
  user = {
    username: 'admin',
    name: 'Administrator'
  }
  res.end()
})

app.post('/logout', (req, res) => {
  user = null
  res.end()
})

const router = jsonServer.router('db.json')
app.use(jsonServer.bodyParser)
app.use(router)

app.listen(3000)
