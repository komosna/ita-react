import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import contactsService from './contacts-service'
import _ from 'lodash'

class ContactListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: [],
      searchText: ''
    }

    this.handleChange = this.handleChange.bind(this);

    this.loadDebounced = _.debounce(this.load, 200) // 1000 to feel strongly the difference
  }

  componentWillMount() {
    this.load(this.state.searchText)
  }

  async load() {
    let res = await contactsService.searchContacts(this.state.searchText)

    this.setState({
      contacts: res.data
    })
  }

  handleChange(event) {
    this.setState({ searchText: event.target.value });

    this.loadDebounced(this.state.searchText);
  }

  render() {
    const contacts = this.state.contacts

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            <input type="text" placeholder="Search contacts" value={this.state.searchText} onChange={this.handleChange} />
          </label>
        </form>

        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>E-mail</th>
              <th>Phone</th>
              <th>City</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {contacts.map(c =>
              <tr key={c.id}>
                <td>
                  <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, { id: c.id })}>{c.name}</Link>
                </td>
                <td>{c.email}</td>
                <td>{c.phone}</td>
                <td>{c.city}</td>
                <td>{('' + c.note).slice(0, 60)}</td>
              </tr>
            )}
          </tbody>
        </table>
        <div>
          <Link to={ROUTES.CONTACT_NEW} className="btn btn-light">Create new</Link>
        </div>
      </div>

    )
  }
}

export default ContactListing
