import React from 'react';

import { HashRouter, Route, Switch, NavLink } from 'react-router-dom'

import authService from './auth-service'

import Login from './login'

import UserListing from './users/user-listing'
import UserEdit from './users/user-edit'
import UserNew from './users/user-new'
import UserDetail from './users/user-detail'

import ContactListing from './contacts/contact-listing'
import ContactEdit from './contacts/contact-edit'
import ContactNew from './contacts/contact-new'
import ContactDetail from './contacts/contact-detail'

import ContractListing from './contracts/contracts-listing'
import ContractEdit from './contracts/contract-edit'
import ContractNew from './contracts/contract-new'
import ContractDetail from './contracts/contract-detail'


import ROUTES from './routes'

const App = () =>
  <HashRouter>
    <div>
      {(!authService.isLoggedIn()) && <Login />}

      {authService.isLoggedIn() && <div>
        <Navigation brand="ContactsApp" />

        <div className="container-fluid">
          <Switch>
            <Route path={ROUTES.USER_DETAIL} component={UserDetail} />
            <Route path={ROUTES.USER_EDIT} component={UserEdit} />
            <Route path={ROUTES.USER_NEW} component={UserNew} />
            <Route path={ROUTES.USER_LISTING} component={UserListing} />

            <Route path={ROUTES.CONTACT_DETAIL} component={ContactDetail} />
            <Route path={ROUTES.CONTACT_EDIT} component={ContactEdit} />
            <Route path={ROUTES.CONTACT_NEW} component={ContactNew} />
            <Route path={ROUTES.CONTACT_LISTING} component={ContactListing} />

            <Route path={ROUTES.CONTRACT_DETAIL} component={ContractDetail} />
            <Route path={ROUTES.CONTRACT_NEW} component={ContractNew} />
            <Route path={ROUTES.CONTRACT_EDIT} component={ContractEdit} />
            <Route path={ROUTES.CONTRACT_LISTING} component={ContractListing} />
            
            <Route path="/" component={ContractListing} />
            

          </Switch>
        </div>
      </div>}
    </div>
  </HashRouter>

const Navigation = (props) =>
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <a className="navbar-brand">{props.brand}</a>

    <div className="d-flex justify-content-between">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink exact className="nav-link" to={ROUTES.CONTRACT_LISTING}>Contracts</NavLink>
        </li>
        <li className="nav-item">
          <NavLink exact className="nav-link" to={ROUTES.CONTACT_LISTING}>Contacts</NavLink>
        </li>
        <li className="nav-item">
          <NavLink exact className="nav-link" to={ROUTES.USER_LISTING}>Users</NavLink>
        </li>
      </ul>

      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <a href="/" className="nav-link" onClick={() => authService.logout()}>Logout</a>
        </li>
      </ul>
    </div>
  </nav>

export default App;
