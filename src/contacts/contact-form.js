import React from 'react'

const ContactForm = ({ contact }) => {
  const handleChange = (e) =>
    contact[e.target.name] = e.target.value

  return (
    <form>
      <div className="form-row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={contact.name} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Email</label>
          <input name="email" type="email" className="form-control" value={contact.email} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Phone</label>
          <input name="phone" type="text" className="form-control" value={contact.phone} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">City</label>
          <input name="city" type="text" className="form-control" value={contact.city} onChange={handleChange} />
        </div>
      </div>
      <div className="form-group">
        <label className="col-form-label">Note</label>
        <textarea name="note" className="form-control" value={contact.note} onChange={handleChange} />
      </div>
    </form>
  )
}

export default ContactForm
