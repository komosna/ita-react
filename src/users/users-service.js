import axios from 'axios'

export default {
  getUsers() {
    return axios.get('http://localhost:3000/users')
  },
// rest up to me

  createUser(data) {
    return axios.post('http://localhost:3000/users', data)    
  },

  getUser(id) {
    return axios.get('http://localhost:3000/users/' + id)    
  },

  updateUser(user1) {
    return axios.put('http://localhost:3000/users/' + user1.id, user1)
    
  }
}

