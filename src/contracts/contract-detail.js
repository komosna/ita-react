import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import svc from './contracts-service'


import Collapsible from '../collapsible'


class ContractDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: {},
      contact: []
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    let res = await svc.getContract(id)

    this.setState({
      contract: res.data,
      contact: res.data.contact
      
    })
  }

  render() {
    const contract = this.state.contract
    const contact = this.state.contact

    return (
      <div>
        <h2>Detail</h2>

        <div className="btn-group">
          <Link className="btn btn-light" to={ROUTES.getUrl(ROUTES.CONTRACT_EDIT, { id: contract.id })}>Edit</Link>
          <Link className="btn btn-danger" to={ROUTES.getUrl(ROUTES.CONTRACT_LISTING)}>Delete</Link>
        </div>

        <table>
          <tbody>
            <tr>
              <th>Name</th>
              <td>{contract.name}</td>
            </tr>
            <tr>
              <th>Description</th>
              <td>{contract.description}</td>
            </tr>
            <tr>
              <th>Price</th>
              <td>{contract.price}</td>
            </tr>
            <tr>
              <th>Contact </th>
              <td><Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, { id: contract.contactId })}>{contact.name}</Link></td>
            </tr>
            <tr>
              <th>Comments</th>
              <td>
                <Collapsible>
                  {contract.comments}
                </Collapsible>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}


export default ContractDetail